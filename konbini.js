const readline = require('readline');

// 商品の値段と税率を定義する
const items = {
    'コーラ': {
        price: 100,
        taxRate: 1.08 // 8%の場合
    },
    'おにぎり': {
        price: 120,
        taxRate: 1.08 // 8%の場合
    },
    'チョコレート': {
        price: 80,
        taxRate: 1.08 // 8%の場合
    },
    '新聞': {
        price: 150,
        taxRate: 1.1 // 10%の場合
    },
};

// 商品名を入力させる
const orders = []; // 購入リスト
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const getOrder = () => {
    return new Promise(resolve => {
        rl.question('商品名を入力してください: ', name => {
                resolve({ name　});
            });
        });
};

const orderMore = () => {
    return new Promise(resolve => {
        rl.question('続けて購入しますか？（はい/いいえ）: ', answer => {
            if (answer === 'はい') {
                resolve(true);
            } else {
                resolve(false);
            }
        });
    });
};

(async () => {
    let isOrdering = true;
    while (isOrdering) {
        const { name } = await getOrder();
        orders.push({ name });

        isOrdering = await orderMore();
    }

    var 小計税抜軽 = 0;
    var 小計税抜重 = 0;

for(let order of orders) {
    const item = items[order.name];
    const taxRate = items[order.taxRate];

    //小計の計算
    if (taxRate === 1.08) {
        //小計(税抜8%)の計算
        var 小計税抜軽 = function () {
            小計税抜軽 = 小計税抜軽 + item.price;
            return 小計税抜軽();

        }
    } else {
        //小計(税抜10%)の計算
        var 小計税抜重 = function() {
            小計税抜重 = 小計税抜重+ item.price;
            return 小計税抜重();
        }
    }
//消費税等を求める
    if (taxRate === 1.08) {
        //消費税等(8%)の計算
        let 消費税等軽 = function(){
            let 税抜値段 = 0;
            税抜値段 = Math.round(item.price * 1.08) - item.price;
            消費税8 = 消費税8 + 税抜値段;
            return 消費税等軽({消費税8});
        }
    } else {
        //消費税等(10%)の計算
        let 消費税等重 = function(){
            let 税抜値段 = 0;
            税抜値段 = Math.round(item.price * 1.1) - item.price;
            消費税10 = 消費税10 + 税抜値段;
            return 消費税等重({消費税10});
        }
    }
}


    console.log("小計(税抜8%) " + 小計税抜軽　+　"円\n");
    console.log("小計(税抜10%) " + 小計税抜重 +　"円\n");
    console.log("消費税等(8%) " + 消費税等軽 + "円\n");
    console.log(+ "消費税等(10%) " + 消費税等重 + "円\n");
    console.log("合計 " + 小計税抜軽 + 小計税抜重 + 消費税等軽 + 消費税等重 + "円");


    rl.close();
})();
